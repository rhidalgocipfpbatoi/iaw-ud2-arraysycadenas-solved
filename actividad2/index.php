<?php

$nombres = array('Pepe', 'Juan', 'Manuel', 'Aitor', 'Ana', 'Laura', 'Sonia', 'Elsa', 'Marta', 'Lucía');

foreach ($nombres as $nombre) {
    echo "<p>$nombre</p>";
}

echo '<p>Número de elementos:'.count($nombres).'</p>';

echo "<p>Nombre aleatorio:" . $nombres[rand(0, 9)] . "</p>";