<?php

$tiradas = [];
$num_caras = 6;

for ($i = 0; $i < $num_caras; $i++) {
    $tiradas[] = 'dado_'.($i+1).'.svg';
}

$indiceCaraSeleccionada = mt_rand(0, count($tiradas)-1);
$caraSeleccionada = $tiradas[$indiceCaraSeleccionada];
$numCaraSeleccionada = $indiceCaraSeleccionada + 1;

echo "<img src='../assets/images/dados/$caraSeleccionada' alt='Cara $numCaraSeleccionada'/>";