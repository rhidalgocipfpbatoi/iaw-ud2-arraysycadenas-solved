<?php

$palabras = [
    'Agua' => 'Water',
    'Fuego' => 'Fire',
    'Máquina' => 'Machine',
    'Gato' => 'Cat',
    'Leche' => 'Milk',
    'Zapato' => 'Shoe'
];

echo "<table>";
echo "<th>Español</th><th>Inglés</th>";
foreach ($palabras as $clave => $valor) {
    echo"<tr><td>$clave</td><td>$valor</td></tr>";
}
echo"</table>";
