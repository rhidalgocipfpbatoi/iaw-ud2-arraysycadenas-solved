<?php
$matriz = [
    [1,2,3,4],
    [5,6,7],
    [9,10,11,12,5],
    [9,10]
];

$mayorValor = 0;

foreach ($matriz as $indice => $listaNumeros) {
    $numElementos = count($listaNumeros);

    if ($numElementos > $mayorValor) {
        $indiceListaMayor = $indice;
        $mayorValor = $numElementos;
    }
}

foreach ($matriz[$indiceListaMayor] as $numero) {
    echo "$numero ";
}